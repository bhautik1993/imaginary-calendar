<?php
require_once 'MyCustomCalendar.php';
$calendar = new MyCustomCalendar();

?>
<!DOCTYPE html>
<html>
<head>
	<title>My Calendar Demo</title>
</head>
<body>
	<form action="" method="POST">
	<table border="0">
		
		<tr>
			<td>
				<select name="year" required="">
					<option value="" disabled selected>Select Year</option>
					<?php for($year = 1990; $year<=2019;$year++){ ?>
						<option <?php if(isset($_POST) && !empty($_POST['year']) && $_POST['year'] == $year){ echo "selected"; } ?>><?=$year?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="month" required="">
					<option value="" disabled selected>Select Month</option>
					<?php for($month = 1; $month<14;$month++){ ?>
						<option <?php if(isset($_POST) && !empty($_POST['month']) && $_POST['month']==$month){ echo "selected"; } ?> ><?=$month?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<select name="day" required="">
					<option value="" disabled selected>Select Day</option>
					<?php for($day = 1; $day<23;$day++){ ?>
						<option <?php if(isset($_POST) && !empty($_POST['day']) && $_POST['day']==$day){ echo "selected"; } ?> ><?=$day?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<button type="submit">Search</button>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>

<?php 
if(isset($_POST) && !empty($_POST)){

	$searchResult = $calendar->searchDay($_POST['day'], $_POST['month'], $_POST['year']);

	echo "Day: ".$searchResult;
}

?>