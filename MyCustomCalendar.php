<?php

class MyCustomCalendar {

    protected $dayName = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

    protected $totalDays;

    protected $startDay;

    protected $startYear = 1990;

    protected $leapYearInterval = 5;

    public function __construct() {
        $this->totalDays = count($this->dayName);
        $this->startDay = array_search('Monday', $this->dayName);
    }

    public function searchDay($day, $month, $year) {

        $checkLeapYear = floor(($year - $this->startYear) / $this->leapYearInterval + 1);
        $currentOffset = $checkLeapYear % $this->totalDays;
        $newIndex = $this->startDay - $currentOffset;
        if ($newIndex < 0) {
            $firstDayYear = $this->startDay + $this->totalDays - $currentOffset;
        } else {
            $firstDayYear = $newIndex;
        }
        $oddMonth = floor($month / 2);
        $firstDayMonth = ($firstDayYear + $oddMonth) % $this->totalDays;
        $resultData = ($firstDayMonth + $day-1) % $this->totalDays;
        return $this->dayName[$resultData];
    }
}